// const MongoClient = require('mongodb').MongoClient;
import {MongoClient, ObjectID} from 'mongodb';
const url = 'mongodb://localhost:27017';
const dbName = 'TodoApp';

// MongoClient.connect(url, (err, client) => {
//   if (err) {
//     return console.log('Unable to connect to MongoDB server');
//   }
//   console.log('Connected to MongoDB server');
//
//   let db = client.db(dbName);
//
//   db.collection('Todos').insertOne({
//       text: 'Walk the dog',
//       completed: true
//   }, (err, result) => {
//     if (err) {
//       return console.log('Unable to insert todo', err);
//     }
//
//     console.log(JSON.stringify(result.ops, undefined, 2));
//   });
//
//   // Insert new doc into Users (name, age, location)
//   // db.collection('Users').insertOne({
//   //   name: 'Andrew',
//   //   age: 25,
//   //   location: 'Philadelphia'
//   // }, (err, result) => {
//   //   if (err) {
//   //     return console.log('Unable to insert user', err);
//   //   }
//   //
//   //   console.log(result.ops[0]._id.getTimestamp());
//   // });
//
//
//   client.close();
// });



let insertUser = (name, age, location, _id) => {
  let objUser = _id ? {
    name,
    age,
    location,
    _id
  } : {
      name,
      age,
      location
  };
    MongoClient.connect(url, (err, client) => {
        if (err) {
            return console.log('Unable to connect to MongoDB server');
        }
        console.log('Connected to MongoDB server');

        let db = client.db(dbName);

        db.collection('Users').insertOne(objUser, (err, result) => {
            if (err) {
                return console.log('Unable to insert todo', err);
            }

            console.log(JSON.stringify(result.ops, undefined, 2));
        });
        client.close();
    });
};

insertUser('Illia', 24, 'Mykolayiv');



