const {MongoClient, ObjectID} = require('mongodb');
const url = 'mongodb://localhost:27017';
const dbName = 'TodoApp';

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    const db = client.db(dbName);

    //deleteOne

    // db.collection('Todos').deleteOne({text: 'Something to do'}).then((res) => {
    //     console.log(res);
    // }, (err) => {
    //     console.log(err);
    // });



    //deleteMany

    // db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((res) => {
    //     console.log(res);
    // }, (err) => {
    //     console.log(err);
    // });



    //findAndDelete
    // db.collection('Todos').findOneAndDelete({completed: false}).then((res) => {
    //     console.log(res);
    // }, (err) => {
    //     console.log(err);
    // });


    // db.collection('Users').deleteMany({name: 'Andrew'}).then((res) => {
    //     console.log(res);
    // });

    db.collection('Users').findOneAndDelete({_id: new ObjectID("5b51ac26eb874e1cede1e4e3")}).then((res) => {
        console.log(res);
    });
    // client.close();
});
