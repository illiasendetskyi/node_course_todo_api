const {MongoClient, ObjectID} = require('mongodb');
const url = 'mongodb://localhost:27017';
const dbName = 'TodoApp';

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');

    const db = client.db(dbName);

    // db.collection('Todos').findOneAndUpdate({
    //     _id: new ObjectID('5b51a9de6db77d1b7b5d8d91')
    // }, {
    //     $set: {
    //         completed: true
    //     }
    // }, {
    //     returnOriginal: false
    // }).then((res) => {
    //     console.log(res);
    // });

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5b51ac0a48ae901cdf9c2ea0')
    },{
        $set: {
            name: 'Alex',
            location: 'Norway'
        },
        $inc: {
            age: +1
        }
    }, {
        returnOriginal: false
    }).then((res) => {
        console.log(res);
    });
    // client.close();
});
