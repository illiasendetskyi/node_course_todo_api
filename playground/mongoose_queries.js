const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo.js');
const {User} = require('./../server/models/user.js');
const{Student} = require('./../server/models/student.js');

// let id = '5b56ddc5e41a4d11e28d077';
//
// if(!ObjectID.isValid(id)) {
//     console.log('ID is not valid');
// }

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// });
//
// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if(!todo) {
//         return console.log('Id not found');
//     }
//     console.log('Todo by "findBYId"', todo)
// }).catch((e) => console.log(e));





 // Student.insertMany([{
 //     name: 'Illia',
 //     age: 26
 // }, {
 //     name: 'Andrew',
 //     age: 21
 // },{
 //     name: 'Mike',
 //     age: 28,
 //     average_rating: 9.6
 // },{
 //     name: 'Alex',
 //     age: 32,
 //     average_rating: 9.9
 // }]).then((res) => {
 //     console.log(res);
 // }, (e) => {
 //     console.log(e);
 // });


let id = '5b56f2a29a7b8c2df9674be2';

if(!ObjectID.isValid(id)) {
    console.log('Id is not valid');
}

Student.findById(id).then((student) => {
    if(!student) {
        return console.log('Student doesn\'t found');
    }
    console.log(`Student: ${student}`)
}).catch((e) => {
    console.log(e);
});

// Student.updateOne({
//     _id: id
// }, {
//     name: 'Mike + 1'
// }).then((res) => {
//     console.log(res);
// }, (e) => {
//     console.log(e);
// });
