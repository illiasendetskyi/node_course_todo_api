const {SHA256} = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

let password = '123abc!';
bcrypt.genSalt(5, (err, salt) => {
    bcrypt.hash(password, salt, (err, hash) => {
       console.log(hash);
    });
});

let hashedPassword10_1 = '$2a$10$fNrawyTVr8S5LJUGCDMJGuyBhtDIZypGtpR5TzCFgZtahy5HIcO0a';
let hashedPassword10_2 = '$2a$10$THKnwR4fF3LxkSGg3IBRFOjebd.q97zJ8kk/XGtuXEt9jQHhiMx8S';
let hashedPassword10_3 = '$2a$10$4zIs8ZiCFmNAYtYqKkANzOVsyHPqXWijnytI7oNRj9NU.6ax7PV0u';
let hashedPassword10_4 = '$2a$10$JUnzsMb5ucyRBsiBS0sLRewPUFHkd3zUjfaO.xBr4rLpLODf8OVf2';
let hashedPassword10_5 = '$2a$10$1TDONGMdoSgGpE6368opGOB7yENVvoBkozfIpLeX9L1GEqLxuVqjG';

let hashedPassword12 = '$2a$12$7OAMQmeXzUOwUuYfPzoYtO1XoTvH0sn307Us2Da15AToUQ4gn9HT2';


bcrypt.compare(password, hashedPassword10_2, (err, res) => {
    console.log(res);
});

// let data = {
//     id: 10
// };
//
// let token = jwt.sign(data, 'abc123');
// console.log(token);
//
// let decoded = jwt.verify(token, 'abc123');
// console.log('decoded', decoded);


// let message = 'I am user number 3';
// let hash = SHA256(message).toString();
//
// console.log(`Message: ${message}`);
// console.log(`Hash: ${hash}`);
//
// let data = {
//   id: 4
// };
//
// let token = {
//     data,
//     hash: SHA256(JSON.stringify(data) + 'somesecret').toString()
// };
//
//
// token.data.id = 5;
// token.hash = SHA256(JSON.stringify(token.data)).toString();
//
//
//
// let resulHash = SHA256(JSON.stringify(token.data) + 'somesecret').toString();
//
// if(resulHash === token.hash) {
//     console.log('Data was not changed');
// } else {
//     console.log('Data was changed. Don\'t trust');
// }

