const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo.js');
const {User} = require('./../server/models/user.js');
const{Student} = require('./../server/models/student.js');

// Todo.remove({}).then((res) => {
//     console.log(res);
// }, (e) => {
//     console.log(e);
// });

Todo.findByIdAndRemove('5b573da4bee046412635e029').then((doc) => {
    console.log(doc);
}, (e) => {
    console.log(e);
});