const {mongoose} = require('./../db/mongoose.js');

let Schema = mongoose.Schema;

let studentSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 1
    },
    age: {
        type: Number,
        required: true,
        trim: true,
        minlength: 1
    },
    average_rating: {
        type: Number
    }
});

let Student = mongoose.model('Student', studentSchema);

module.exports = {
    Student
};